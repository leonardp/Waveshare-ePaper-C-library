# Portable embedded graphics library for Waveshare ePaper displays (SPI)
This is a portable library targeted for microcontrollers driving ePaper displays. The library can display bitmaps and 
proportional text. There are no dependencies - just pure C. It comes with a bitmap converter written in Python to 
generate the necessary C code.
# More information
based on:

 - [Detailed description of the library](http://lb9mg.no/2018/02/10/waveshare-epaper-display-library/)
- [How to make your own graphics and fonts](http://lb9mg.no/2018/02/10/making-graphics-and-fonts-for-embedded-systems/)
