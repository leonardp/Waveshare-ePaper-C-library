#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_wifi.h"
#include "esp_deep_sleep.h"
#include "nvs_flash.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"

#include "main.h"
#include "epd.h"
#include "resources.h"

RTC_DATA_ATTR int framer;

void RTC_IRAM_ATTR esp_wake_deep_sleep(void) {
    esp_default_wake_deep_sleep();
    framer++;
    if (framer > 5)
    {
      framer = 0;
    }
}


void app_main()
{
    /*
    nvs_flash_init();
    tcpip_adapter_init();
    ESP_ERROR_CHECK( esp_event_loop_init(event_handler, NULL) );
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    wifi_config_t sta_config = {
        .sta = {
            .ssid = "",
            .password = "",
            .bssid_set = false
        }
    };
    ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_STA, &sta_config) );
    ESP_ERROR_CHECK( esp_wifi_start() );
    ESP_ERROR_CHECK( esp_wifi_connect() );
    */

    // initialize SPI
    spi_device_handle_t epd_spi;
    epd_spi = epd_spi_init();
    
    //Initialize the e-Paper Display
    epd_init(epd_spi);
    printf("Display Initialized\n");

    //epd_clear(epd_spi);
    //epd_spi_cmd(epd_spi, DISPLAY_REFRESH);
    //epd_busy_wait();

    //const unsigned char *frames[] = {
    //        BITMAP_fractal_1.payload,
    //        BITMAP_fractal_2.payload,
    //        BITMAP_fractal_3.payload,
    //        BITMAP_fractal_4.payload,
    //        BITMAP_fractal_5.payload
    //};

    const unsigned char *frames[] = {
            BITMAP_fractal_1.payload,
            BITMAP_fractal_3.payload,
            BITMAP_fractal_5.payload
    };

    // heartbeat
    gpio_set_direction(GPIO_NUM_22, GPIO_MODE_OUTPUT);
    int level = 0;

    const int wakeup_time_sec = 45;
    esp_deep_sleep_enable_timer_wakeup(wakeup_time_sec * 1000000);

    while (true) {
        /*
        gpio_set_level(GPIO_NUM_22, level);
        level = !level;
        vTaskDelay(300 / portTICK_PERIOD_MS);
        */

        printf("frame nr: %d\n", framer);
        epd_display_fb(epd_spi, frames[framer], NULL);
        esp_deep_sleep_start();
    }
}

