#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "esp_event.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"

/*
 * Definitions
 */
/*
#define PIN_NUM_MISO 4
#define PIN_NUM_MOSI 16
#define PIN_NUM_CLK  17
#define PIN_NUM_CS   5

#define PIN_NUM_DC   18
#define PIN_NUM_RST  23
#define PIN_NUM_BUSY 19
*/

#define PIN_NUM_MISO 34
#define PIN_NUM_MOSI 26
#define PIN_NUM_CLK  25
#define PIN_NUM_CS   33

#define PIN_NUM_DC   32
#define PIN_NUM_RST  27
#define PIN_NUM_BUSY 35

//To speed up transfers, every SPI transfer sends a bunch of lines. This define specifies how many. More means more memory use,
//but less overhead for setting up / finishing transfers. Make sure 240 is dividable by this.
#define PARALLEL_LINES 16
#define MAX_TRANSFER_SIZE PARALLEL_LINES*320*2+8

#define SPI_CLOCK_SPEED 20*1000*1000 //Clock out at 20 MH

/*
#ifdef CONFIG_EPD_SIZE_15
#define EPD_WIDTH  128
#define EPD_HEIGHT 296
#elif defined ( CONFIG_EPD_SIZE_42 )
#define EPD_WIDTH  400
#define EPD_HEIGHT 300
#endif
*/
#define EPD_WIDTH  400
#define EPD_HEIGHT 300

// EPD 4,2" commands
#define PANEL_SETTING                  0x00
#define POWER_SETTING                  0x01
#define POWER_OFF                      0x02
#define POWER_OFF_SEQUENCE_SETTING     0x03
#define POWER_ON                       0x04
#define POWER_ON_MEASURE               0x05
#define BOOSTER_SOFT_START             0x06
#define DEEP_SLEEP                     0x07
#define DATA_START_TRANSMISSION_1      0x10
#define DATA_STOP                      0x11
#define DISPLAY_REFRESH                0x12
#define DATA_START_TRANSMISSION_2      0x13
#define VCOM_LUT                       0x20
#define W2W_LUT                        0x21
#define B2W_LUT                        0x22
#define W2B_LUT                        0x23
#define B2B_LUT                        0x24
#define PLL_CONTROL                    0x30
#define TEMPERATURE_SENSOR_CALIBRATION 0x40
#define TEMPERATURE_SENSOR_SELECTION   0x41
#define TEMPERATURE_SENSOR_WRITE       0x42
#define TEMPERATURE_SENSOR_READ        0x43
#define VCOM_AND_DATA_INTERVAL_SETTING 0x50
#define LOW_POWER_DETECTION            0x51
#define TCON_SETTING                   0x60
#define RESOLUTION_SETTING             0x61
#define GSST_SETTING                   0x65
#define GET_STATUS                     0x71
#define AUTO_MEASURE_VCOM              0x80
#define VCOM_VALUE                     0x81
#define VCM_DC_SETTING                 0x82
#define PARTIAL_WINDOW                 0x90
#define PARTIAL_IN                     0x91
#define PARTIAL_OUT                    0x92
#define PROGRAM_MODE                   0xA0
#define ACTIVE_PROGRAM                 0xA1
#define READ_OTP_DATA                  0xA2
#define POWER_SAVING                   0xE3

/*
 * Type definitions
 */

typedef struct {
    uint8_t cmd;
    uint8_t data[16];
    uint8_t databytes; //No of data in data; bit 7 = delay after set; 0xFF = end of cmds.
} epd_cmd_seq_t;


/*
 * Variables
 */
IRAM_ATTR extern uint8_t GLOBAL_framebuffer[EPD_HEIGHT][EPD_WIDTH / 8];
//              this is Y (longer edge) --- ^^          ^^ --- this is X (shorter edge)

//Place data into DRAM. Constant data gets placed into DROM by default, which is not accessible by DMA.
DRAM_ATTR static const epd_cmd_seq_t epd_init_cmds[]={
  // booster soft start
  {BOOSTER_SOFT_START, {0x17, 0x17, 0x17}, 3},
  // power on
  {POWER_ON, {0}, 0x80},
  
  {0, {0}, 0xFF}
};

DRAM_ATTR static const epd_cmd_seq_t epd_config_cmds[]={

    // panel setting
    {PANEL_SETTING, {0x0F}, 1},

    {0, {0}, 0xFF}
};

/*
 * Functions
 */
spi_device_handle_t epd_spi_init(void);
void epd_spi_pre_transfer_callback(spi_transaction_t *t);


/**
* Sends data over the SPI bus to the display. This function is blocking.
* @param data Pointer to data to be written.
* @param length Length of data to be written (in bytes)
*/
void epd_spi_cmd(spi_device_handle_t spi, const uint8_t cmd);
void epd_spi_data(spi_device_handle_t spi, const uint8_t *data, uint32_t length);

/**
 * Polls the BUSY signal from the display. This function is blocking.
 * TODO:
 * It can either use polling or configure the GPIO for interrupt sensing.
 * This function should also enter sleep mode while waiting for reduced power consumption.
 */
void epd_busy_wait(void);

void epd_reset(void);


void epd_init(spi_device_handle_t spi);
void epd_clear(spi_device_handle_t spi);
void epd_display_fb(spi_device_handle_t spi, const unsigned char* frame_black, const unsigned char* frame_red);
void epd_refresh(spi_device_handle_t spi);
void epd_send_sequence(spi_device_handle_t spi, const epd_cmd_seq_t *cmds);

void epd_deinit(void);
